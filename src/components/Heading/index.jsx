import { useCounterContext } from '../../contexts';

export const Heading = () => {
  const [state, actions] = useCounterContext();

  return <h1> {state.counter}</h1>;
};
