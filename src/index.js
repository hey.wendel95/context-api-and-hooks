import React from 'react';
import ReactDOM from 'react-dom';
import { CounterContextProvider } from './contexts';
import './styles/global-styles.css';

import { Home } from './templates/Home';

ReactDOM.render(
  <CounterContextProvider>
    <React.StrictMode>
      <Home />
    </React.StrictMode>
  </CounterContextProvider>,
  document.getElementById('root'),
);
